public class exception
{
    public static void main(String[] args)
    {
        String txt = "Это тестовое предложение, в котором одно из слов будетсостоятьболеечемиздесятибукв.";
        String[] words = txt.replaceAll("[^A-Za-zА-Яа-я-\s]", "").split(" ");
        System.out.println("Слово состоящее более чем из десяти букв выделено \u001b[31mкрасным \u001b[0mцветом");
        for (String word:words)
        {
            try
            {
                if (word.length() > 10)
                {
                    throw new IndexOutOfBoundsException();
                }

                System.out.print(word + " ");
            } catch (IndexOutOfBoundsException e)
            {
                System.out.print("\u001b[31m" + word + "\u001b[0m ");
            }
        }
    }
}

